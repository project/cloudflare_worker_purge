<?php

namespace Drupal\cloudflare_worker_purge\Plugin\Purge\Purger;

use Drupal\Core\Config\Config;
use Drupal\key\KeyRepositoryInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\Utils;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Cloudflare Worker purger.
 *
 * @PurgePurger(
 *   id = "cloudflare_worker",
 *   label = @Translation("Cloudflare Worker"),
 *   description = @Translation("Purger for Cloudflare Worker."),
 *   types = {"tag"},
 *   multi_instance = FALSE,
 *   configform = "\Drupal\cloudflare_worker_purge\Form\CloudflareWorkerPurgeForm",
 * )
 */
class CloudflareWorkerPurger extends PurgerBase {

  public const MAX_TAG_PURGES_PER_REQUEST = 30;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected Config $config,
    protected ClientInterface $client,
    LoggerInterface $logger,
    protected ?KeyRepositoryInterface $keyRepository = NULL,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setLogger($logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('cloudflare_worker_purge.settings'),
      $container->get('http_client'),
      $container->get('logger.factory')->get('cloudflare_worker_purge'),
      $container->get('key.repository', Container::NULL_ON_INVALID_REFERENCE)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getIdealConditionsLimit() {
    return self::MAX_TAG_PURGES_PER_REQUEST * 10;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    if (empty($invalidations)) {
      return;
    }

    $url = $this->config->get('url');

    if (empty($url)) {
      $this->logger->error('Purge URL not set');

      // Set the state of each invalidation.
      foreach ($invalidations as $invalidation) {
        $invalidation->setState(InvalidationInterface::FAILED);
      }

      return;
    }

    // Group the messages by tag.
    /** @var array<string,InvalidationInterface[]> */
    $grouped = array_reduce($invalidations, function ($carry, $item) {
      $expression = $item->getExpression();
      if (empty($expression) || !is_string($expression)) {
        $item->setState(InvalidationInterface::NOT_SUPPORTED);
        return $carry;
      }
      if (!array_key_exists($expression, $carry)) {
        $carry[$expression] = [$item];
      }
      else {
        $carry[$expression][] = $item;
      }

      return $carry;
    }, []);

    /** @var array<int,array<string,InvalidationInterface[]>> */
    $chunks = array_chunk($grouped, self::MAX_TAG_PURGES_PER_REQUEST, TRUE);

    /** @var array<string,string> */
    $headers = [];
    if ($this->keyRepository && $this->config->get('token')) {
      $token = $this->keyRepository->getKey($this->config->get('token'))->getKeyValue();
      if ($token) {
        $headers['Authorization'] = "Bearer $token";
      }
    }

    $promises = array_map(function ($chunk) use ($url, $headers) {
      return $this->client->requestAsync('POST', $url, [
        'headers' => $headers,
        'json' => [
          'tags' => array_keys($chunk),
        ],
      ])->then(function () use ($chunk) {
        foreach ($chunk as $invalidations) {
          foreach ($invalidations as $invalidation) {
            $invalidation->setState(InvalidationInterface::SUCCEEDED);
          }
        }
      }, function (\Throwable $e) use ($chunk) {
        $this->logger->critical($e->getMessage());

        foreach ($chunk as $invalidations) {
          foreach ($invalidations as $invalidation) {
            $invalidation->setState(InvalidationInterface::FAILED);
          }
        }
      });
    }, $chunks);

    // Execute the promises.
    Utils::unwrap($promises);
  }

}
