<?php

namespace Drupal\cloudflare_worker_purge\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\purge_ui\Form\PurgerConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Cloudflare Worker Purge Configuration Form.
 */
class CloudflareWorkerPurgeForm extends PurgerConfigFormBase {

  /**
   * Configuration Name.
   *
   * @var string
   */
  private const CONFIG_NAME = 'cloudflare_worker_purge.settings';

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected ModuleHandlerInterface $moduleHandler,
    protected $typedConfigManager = NULL,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_worker_purge.purger_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);
    $form['url'] = [
      '#title' => $this->t('URL'),
      '#type' => 'url',
      '#default_value' => $config->get('url'),
      '#description' => $this->t('The purger with make a request to the URL in the same format as the <a href="@cache_api">Purge Cache API</a>.', [
        "@cache_api" => 'https://api.cloudflare.com/#zone-purge-files-by-cache-tags,-host-or-prefix',
      ]),
    ];

    if ($this->moduleHandler->moduleExists('key')) {
      $form['token'] = [
        '#title' => $this->t('API Token'),
        '#type' => 'key_select',
        '#default_value' => $config->get('token'),
        '#description' => $this->t('This could be a <a href="@api_token">Cloudflare API Token</a>, or any authorization your Cloudflare Worker accepts.', [
          '@api_token' => 'https://developers.cloudflare.com/fundamentals/api/get-started/create-token/',
        ]),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    $this->config(self::CONFIG_NAME)
      ->set('url', $form_state->getValue('url'))
      ->set('token', $form_state->getValue('token'))
      ->save();
  }

}
