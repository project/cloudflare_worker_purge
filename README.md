# Cloudflare Worker Purge
The Cloudflare Worker Purge module integrates with [Purge](https://www.drupal.org/project/purge) in order to send purge
requests to a custom [Cloudflare Worker](https://developers.cloudflare.com/workers/) like
[Cache Tag](https://github.com/davidbarratt/cache-tag).

> [!IMPORTANT]
> By the time a response from an origin reaches a [Worker](https://developers.cloudflare.com/workers/), Cloudflare
> has already swallowed the `Cache-Tag` header and it is no longer available. To get around this, this modules sends a
> custom `X-Cache-Tag` header instead.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements
- [Purge](https://www.drupal.org/project/purge)
- A custom [Cloudflare Worker](https://developers.cloudflare.com/workers/) like
  [Cache Tag](https://github.com/davidbarratt/cache-tag)

## Recommended modules
- [Key](https://www.drupal.org/project/key) (if using an API Key to authenticate). You **should** use _something_ to
  authenticate requests to the worker, otherwise anyone can make purge requests.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module accepts a URL that is used for purge requests and, if the [Key](https://www.drupal.org/project/key) module is
installed, an optional API Key to pass to the [Cloudflare Worker](https://developers.cloudflare.com/workers/)
